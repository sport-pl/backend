module.exports = {
  '*.{js,ts}': ['eslint --fix', 'eslint'],
  '**/*.ts?(x)': () => 'yarn run check-types',
  '*.{json,yaml}': ['prettier --write'],
};
