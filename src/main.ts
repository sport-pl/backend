import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ConfigService } from '@nestjs/config';
import { ServerConfig } from '@config/server.config';
import { ValidationPipe } from '@nestjs/common';
import { AllExceptionsFilter } from '@filter/all-exception.filter';
import { SwaggerModule } from '@nestjs/swagger';
import { SwaggerConfig } from '@config/swagger.config';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const config = app.get(ConfigService);
  const serverConfig = config.get<ServerConfig>('server');
  app.useGlobalPipes(new ValidationPipe());
  const document = SwaggerModule.createDocument(
    app,
    SwaggerConfig.getDocumentConfig(),
  );
  SwaggerModule.setup('api/document', app, document);
  // }
  app.useGlobalFilters(new AllExceptionsFilter());
  app.enableCors();

  await app.listen(serverConfig.port);
}
bootstrap()
  .then(() => {
    console.log('success!');
  })
  .catch(() => {
    console.log('error!');
  });
