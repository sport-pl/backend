import { Injectable } from '@nestjs/common';
import { InjectRedis, Redis } from '@nestjs-modules/ioredis';

@Injectable()
export class AuthService {
  constructor(
    @InjectRedis()
    private readonly redisService: Redis,
  ) {}

  async storeToken(key: string, token: string) {
    await this.redisService.set(key, token, 'EX', 3600); // set token with expiry of 1 hour
  }

  async getToken(key: string): Promise<string | null> {
    return await this.redisService.get(key);
  }

  async removeToken(key: string) {
    await this.redisService.del(key);
  }
}
