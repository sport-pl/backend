import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity('users')
export class UserEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  name: string;

  @Column({ type: 'date', nullable: true })
  dob: string;

  @Column({ nullable: true })
  phone: string;

  @Column({ unique: true, nullable: true })
  email: string;

  @Column()
  password: string;

  @Column({ nullable: true })
  avatar: string;

  @Column({ default: false })
  isDelete: boolean;

  @Column({ nullable: true, default: false })
  isDefaultPassword: boolean;

  // @OneToMany(() => TokenEntity, (token) => token.user)
  // tokens: Promise<TokenEntity[]>;
}
