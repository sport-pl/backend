import * as Redis from 'ioredis';

// export declare type Redis = Redis.Redis;

export interface RedisModuleOptions {
  config: Redis.RedisOptions & {
    url?: string;
  };
}
export interface RedisConfig {
  url: string;
}
export const REDIS_PATH_CONFIG = 'redis';
