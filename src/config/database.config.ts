import { DataSourceOptions } from 'typeorm';

export type DatabaseConfig = DataSourceOptions;
export const DB_PATH_CONFIG = 'db';
