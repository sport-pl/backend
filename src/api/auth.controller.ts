import { AuthService } from '@auth/auth.service';
import { Controller, Post, Request, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';

@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @UseGuards(AuthGuard('local'))
  @Post('login')
  async login(@Request() req) {
    // generate JWT token...
    const token = '...';
    // store JWT token in Redis
    await this.authService.storeToken(req.user.id, token);
    return { token };
  }
}
